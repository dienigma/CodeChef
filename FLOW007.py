rev=0
t = int(input())
for i in range(t):
  n=int(input())
  while(n>0):
    remainder = n%10
    rev = (rev*10)+remainder
    n = n//10
  print(rev)
  rev = 0