t=int(input())
for i in range(t):
	w=1
	l=2
	mdiff=0
	si,ti=map(int,input().split(" "))
	diff=si-ti
	if abs(mdiff)<abs(diff):
		mdiff=diff
	if mdiff>0:
		print(str(w)+str(abs(mdiff)))
	if mdiff<0:
		print(str(l)+str(abs(mdiff)))
